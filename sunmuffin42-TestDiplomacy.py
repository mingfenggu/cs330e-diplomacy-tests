#!/usr/bin/env python3
"""
Ensure Diplomacy module works as expected

Classes:
    TestDplomacy: unit tests for Diplomacy.py
"""
# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a.name, "A")
        self.assertEqual(a.location, "Madrid")
        self.assertEqual(a.action, "Hold")
        self.assertEqual(a.target, "")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        a = diplomacy_read(s)
        self.assertEqual(a.name, "B")
        self.assertEqual(a.location, "Madrid")
        self.assertEqual(a.action, "Move")
        self.assertEqual(a.target, "Madrid")

    def test_read_3(self):
        s = "C London Support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a.name, "C")
        self.assertEqual(a.location, "London")
        self.assertEqual(a.action, "Support")
        self.assertEqual(a.target, "B")
        self.assertEqual(a.supporting, "B")

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        a = Army("A", "Madrid", "Hold")
        b = Army("B", "Barcelona", "Move", "Madrid")
        b.living = False
        armies = [a, b]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        a = Army("D", "Paris", "Support", "B")
        b = Army("B", "Barcelona", "Move", "Madrid")
        b.living = False
        armies = [a, b]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "D Paris\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        a = Army("D", "Austin", "Move", "London")
        b = Army("C", "London", "Support", "B")
        a.living = False
        b.living = False
        armies = [a, b]
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "D [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
